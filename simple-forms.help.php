<?php ?>
<style>
  div#help-wrapper {box-sizing: border-box;padding: 20px;width: 75%;}
  table {border: none;}
  tr {vertical-align: top;}
  td {vertical-align: top; padding: 0 10px 10px;}
  pre {font-family: Courier, monospace;background: white;padding: 10px;border-radius: 5px;color: navy;}
  pre em {color: red;font-style: initial;}
  hr {text-align: center;width: 80%;}
  ul li {list-style:square;margin:0 0 0 1.4em;}
  li.has-child ul li {list-style:circle;}
  ul.wp-submenu {margin:0;}
  .top {text-align: right;margin: 0 20px;}
  h2 {margin: 60px 0 20px;}
  a {text-decoration: none;}
  a:hover {text-decoration: underline;}
  a strong {color: darkslateblue;}
  .content-wrapper {position: fixed;top: 40px;right: 40px;background: white;font-size: 0.8em;line-height: 1.4em;padding: 0 10px;border-radius: 5px;}
  .footer-note {font-size: 0.75em;font-style: italic;}
  .admin_page_simpleforms-help label {vertical-align: top;font-weight: 700;color: #444;padding: 0 10px;line-height: 28px;}
  .admin_page_simpleforms-help input {border: 1px solid #444;}
  .admin_page_simpleforms-help textarea {border: 1px solid #444;}
</style>
<a name="top"></a>
<div id='help-wrapper'>
  <h1>Simple Forms Help Page</h1>
  <div>The following will help you use the simple-forms plugin.</div>
  <div class="content-wrapper">
    <h4>Contents:</h4>
    <ul>
      <li><a href="#bf">Basic functionality</a></li>
      <li class="has-child"><a href="#ctfa">Creating the $form array</a>
        <ul>
          <li><a href="#fd">$form definitions</a></li>
          <li><a href="#ffnd">$form functions definitions</a></li>
          <li><a href="#ffd">$form fields definitions</a></li>
        </ul>
      </li>
      <li class="has-child"><a href="#dfd">Detailed field description by types</a>
        <ul>
          <li><a href="#button">button</a></li>
          <li><a href="#checkbox">checkbox</a></li>
          <li><a href="#checkboxes">checkboxes</a></li>
          <li><a href="#custom">custom</a></li>
          <li><a href="#date-popup">date-popup</a></li>
          <li><a href="#hidden">hidden</a></li>
          <li><a href="#markup">markup</a></li>
          <li><a href="#phone">phone</a></li>
          <li><a href="#radio">radio</a></li>
          <li><a href="#radios">radios</a></li>
          <li><a href="#select">select</a></li>
          <li><a href="#submit">submit</a></li>
          <li><a href="#text">text</a><br/>
            Other text field types:
            <ul>
              <li><a href="#text2">date</a></li>
              <li><a href="#text2">datetime-local</a></li>
              <li><a href="#text2">email</a></li>
              <li><a href="#text2">number</a></li>
              <li><a href="#text2">password</a></li>
            </ul>
          </li>
          <li><a href="#textarea">textarea</a></li>
          <li><a href="#title">title</a></li>
        </ul>
      </li>
      <li class="has-child"><a href="#ofk">Optional field keys</a>
        <ul>
          <li><a href="#attributes">attributes</a></li>
          <li><a href="#class">class</a></li>
          <li><a href="#cols">cols</a></li>
          <li><a href="#default_value">default_value</a></li>
          <li><a href="#item_prefix">item_prefix</a></li>
          <li><a href="#item_suffix">item_suffix</a></li>
          <li><a href="#label">label</a></li>
          <li><a href="#maxlength">maxlength</a></li>
          <li><a href="#placeholder">placeholder</a></li>
          <li><a href="#prefix">prefix</a></li>
          <li><a href="#required">required</a></li>
          <li><a href="#rows">rows</a></li>
          <li><a href="#size">size</a></li>
          <li><a href="#suffix">suffix</a></li>
        </ul>
      </li>
    </ul>
  </div>
  <a name="bf"></a>
  <h2>Basic functionality</h2>
  <div>To create a form you'll need to create a new object:</div>
  <div>The <em><a href="#ctfa">$form</a></em> array includes the form definition.</div>
  <pre>$new_obj = new simple_form($form);</pre>
  <div>To render and show the form do:</div>
  <pre>print $new_obj->render_form();</pre>
  <div>&nbsp;</div>
  <div>To submit the form do:</div>
  <div><em>Note: Form submission also validate the form.</em></div>
  <pre>print $new_obj->submit_form();</pre>
  <div>&nbsp;</div>
  <div>To only validate the form do:</div>
  <pre>print $new_obj->validate_form();</pre>
  <div>&nbsp;</div>
  <div class="top"><a href="#top">Back to top</a></div>
  <hr />
  <div>&nbsp;</div>

  <a name="ctfa"></a>
  <h2>Creating the $form array</h2>
  <div>The form array includes general form definitions and the various fields definitions.</div>

  <a name="fd"></a>
  <h3>$form definitions</h3>
  <table>
    <tr>
      <td width='10%'><strong>type: </strong></td>
      <td>The type of form (optional). If not set, it is assumed to be <em>admin_settings</em>.<br />An <em>admin_settings</em> form is a special case form and does not need a submit field and/or submit handling. The plugin takes care of the form submission, validation of fields (required on the server side), and saving fields as options in Wordpress options table</td>
    </tr>
    <tr>
      <td><strong>id: </strong></td>
      <td>The form id (optional). If no form id is defined, the form id will default to form-XXXXXX, where XXXXXX is the unix timestamp.</td>
    </tr>
    <tr>
      <td><strong>class: </strong></td>
      <td>The form class (optional).</td>
    </tr>
    <tr>
      <td><strong>action: </strong></td>
      <td>The form action (optional). If action is not defined, the form URI is set as the action.</td>
    </tr>
    <tr>
      <td><strong>method: </strong></td>
      <td>The form method (optional). If method is not defined, the form method is set to POST.</td>
    </tr>
    <tr>
      <td><strong>redirect: </strong></td>
      <td>TBD</td>
    </tr>
    <tr>
      <td><strong>prefix: </strong></td>
      <td>TBD</td>
    </tr>
    <tr>
      <td><strong>suffix: </strong></td>
      <td>TBD</td>
    </tr>
    <tr>
      <td><strong>token: </strong></td>
      <td>TBD</td>
    </tr>
    <tr>
      <td><strong>colon: </strong></td>
      <td>Whether to place colon after the field label (optional).<br />Boolean<br />colon is set to TRUE by default.</td>
    </tr>
  </table>
  <div>&nbsp;</div>
  <div>An example for $form definition:</div>
  <pre>
&lt;?php
$form = array(
  'type' => 'admin_settings',
  'id' => 'simple-forms-settings',
  'colon' => FALSE,
);
?>
  </pre>

  <a name="ffnd"></a>
  <h3>$form functions definitions</h3>
  <table>
    <tr>
      <td width='10%'><strong>presubmit_callback: </strong></td>
      <td>Set a function callback (single or an array of functions) that will be called before the form is saved.<br/>These callbacks allow manipulation of the form submitted data.</td>
    </tr>
    <tr>
      <td><strong>postsubmit_callback: </strong></td>
      <td>Set a function callback (single or an array of functions) that will be called after the form is saved. </td>
    </tr>
  </table>
  <div>&nbsp;</div>
  <div>An example for $form functions definition:</div>
  <pre>
&lt;?php
$form = array(
  'type' => 'admin_settings',
  'id' => 'simple-forms-settings',
  'presubmit_callback' => array(
    '_custom_submit_callback',
  ),
);

function _custom_submit_callback(&$form) {
  if ($form['id'] == 'simple-forms-settings') {
    // Do something ...
  }
}
?>
  </pre>

  <a name="ffd"></a>
  <h3>$form fields definitions</h3>
  <div>A field starts with its name. The name is the key in the $form array. A field definition is an array.<br />Here we define a new field who's name is <em>first_name</em>.</div>
  <pre>$form['first_name'] = array();</pre>
  <div>The field type defines the type of field we need to create. If a field type is not defined, it is assumed to be markup. In that case, the <em>markup</em> key MUST be set.</div>
  <pre>&lt;?php
$form['first_name'] = array(
  'type' => 'text',
);
$form['help_text'] = array(
  <em>'markup' => '&lt;p>Some markup text ...&lt;/p>',</em>
);
?>
  </pre>
  <div>
    These are the supported field types: <br/>
    <a href='#button'>button</a>,
    <a href='#checkbox'>checkbox</a>,
    <a href='#checkboxes'>checkboxes</a>,
    <a href='#custom'>custom</a>,
    <a href='#text2'>date</a>, 
    <a href='#date-popup'>date-popup</a>, 
    <a href='#text2'>datetime-local</a>, 
    <a href='#text2'>email</a>,
    <a href='#hidden'>hidden</a>,
    <a href='#markup'>markup</a>, 
    <a href='#text2'>number</a>, 
    <a href='#text2'>password</a>, 
    <a href='#phone'>phone</a>, 
    <a href='#radio'>radio</a>, 
    <a href='#radios'>radios</a>,
    <a href='#select'>select</a>,
    <a href='#submit'>submit</a>,
    <a href='#text'>text</a>,
    <a href='#textarea'>textarea</a>,
    <a href='#title'>title</a>
  </div>

  <div>&nbsp;</div>
  <div class="top"><a href="#top">Back to top</a></div>
  <hr />
  <div>&nbsp;</div>

  <a name="dfd"></a>
  <h2>Detailed field description by types</h2>
  <div><em>Bold keys (in available field keys) are mandatory for the specified type. Mandatory field are on top of the type field.</em></div>

  <a name="text"></a>
  <h3>text</h3>
  <div>The text field type is used to define a simple simple text input control.</div>
  <div>The field require the <strong>type</strong> and <strong>label</strong> as its minimum attributes.</div>
  <pre>&lt;?php
$form['first_name'] = array(
  <em>'type' => 'text',</em>
  'label' => 'Enter first name',
);
?>
  </pre>
  <div>Available field keys: <br/>
    <a href='#attributes'>attributes</a>, 
    <a href='#class'>class</a>, 
    <a href='#default_value'>default_value</a>, 
    <a href='#item_prefix'>item_prefix</a>, 
    <a href='#item_suffix'>item_suffix</a>, 
    <a href='#label'><strong>label</strong></a>, 
    <a href='#maxlength'>maxlength</a>, 
    <a href='#placeholder'>placeholder</a>, 
    <a href='#prefix'>prefix</a>, 
    <a href='#required'>required</a>, 
    <a href='#size'>size</a>, 
    <a href='#suffix'>suffix</a>, 
  </div>

  <a name="text2"></a>
  <h3>date, datetime-local, email, number, password</h3>
  <div>All these fields are used to define text input control that differ from the regular text control by the type of data entered and verified (by the browser) to them and only apply to browsers that support HTML5 field types.</div>
  <div>Like the text field, these fields require the <strong>type</strong> and <strong>label</strong> as their minimum attributes.</div>
  <pre>&lt;?php
$form['password'] = array(
  <em>'type' => 'password',</em>
  'label' => 'Enter password',
);
?>
  </pre>
  <div>Available field keys: <br/>
    <a href='#attributes'>attributes</a>, 
    <a href='#class'>class</a>, 
    <a href='#default_value'>default_value</a>, 
    <a href='#item_prefix'>item_prefix</a>, 
    <a href='#item_suffix'>item_suffix</a>, 
    <a href='#label'><strong>label</strong></a>, 
    <a href='#maxlength'>maxlength</a>, 
    <a href='#placeholder'>placeholder</a>, 
    <a href='#prefix'>prefix</a>, 
    <a href='#required'>required</a>, 
    <a href='#size'>size</a>, 
    <a href='#suffix'>suffix</a> 
  </div>

  <a name="textarea"></a>
  <h3>textarea</h3>
  <div>The textarea field type is used to define a multi-line text input control.</div>
  <div>The field require the <strong>type</strong> and <strong>label</strong> as its minimum attributes.</div>
  <pre>&lt;?php
$form['body'] = array(
  <em>'type' => 'textarea',</em>
  'label' => 'Enter body text',
  'rows' => 10,
  'cols' => 60,
);
?>
  </pre>
  <div>Available field keys: <br/>
    <a href='#attributes'>attributes</a>, 
    <a href='#class'>class</a>, 
    <a href='#cols'>cols</a>, 
    <a href='#default_value'>default_value</a>, 
    <a href='#item_prefix'>item_prefix</a>, 
    <a href='#item_suffix'>item_suffix</a>, 
    <a href='#label'><strong>label</strong></a>, 
    <a href='#maxlength'>maxlength</a>, 
    <a href='#placeholder'>placeholder</a>, 
    <a href='#prefix'>prefix</a>, 
    <a href='#required'>required</a>, 
    <a href='#rows'>rows</a>, 
    <a href='#suffix'>suffix</a>, 
  </div>

  <div>&nbsp;</div>
  <div class="top"><a href="#top">Back to top</a></div>
  <hr />
  <div>&nbsp;</div>

  <a name="ofk"></a>
  <h2>Optional field keys</h2>

  <a name="attributes"></a>
  <h3>attributes (optional)</h3>
  <div>This key is used to add any desired attribute to the field.</div>
  <div>The attributes are set as key / value pairs.</div>
  <pre>&lt;?php
$form['car_number'] = array(
  'type' => 'text',
  'label' => 'Car number',
  <em>'attributes' => array(
    'data-timestamp' => time(),
    'placeholder' => 'Car number',
  ),</em>
);
?>
  </pre>
  <label for="car_number">Car number: </label><input type="text" name="car_number" id="car_number" value="" class="" data-timestamp="<?= time() ?>" placeholder="Car number">
  <div>&nbsp;</div>
  <div>Used in field types: <br/>
    <a href='#button'>button</a>,
    <a href='#checkbox'>checkbox</a>,
    <a href='#checkboxes'>checkboxes</a>,
    <a href='#custom'>custom</a>,
    <a href='#hidden'>hidden</a>,
    <a href='#text2'>date</a>, 
    <a href='#date-popup'>date-popup</a>, 
    <a href='#text2'>datetime-local</a>, 
    <a href='#text2'>email</a>, 
    <a href='#text2'>number</a>, 
    <a href='#text2'>password</a>, 
    <a href='#phone'>phone</a>, 
    <a href='#radio'>radio</a>, 
    <a href='#radios'>radios</a>,
    <a href='#select'>select</a>,
    <a href='#submit'>submit</a>,
    <a href='#text'>text</a>,
    <a href='#textarea'>textarea</a>,
    <a href='#title'>title</a>
  </div>

  <a name="class"></a>
  <h3>class (optional)</h3>
  <div>This key is used to add classes to the field.</div>
  <div>The key can have a string or an array of classes. The later is preferable.</div>
  <pre>&lt;?php
$form['password'] = array(
  'type' => 'password',
  'label' => 'Enter password',
  <em>'class' => array(
    'password-class-1',
    'password-class-2',
  ),</em>
);
?>
  </pre>
  <div>Used in field types: <br/>
    <a href='#button'>button</a>,
    <a href='#checkbox'>checkbox</a>,
    <a href='#checkboxes'>checkboxes</a>,
    <a href='#custom'>custom</a>,
    <a href='#hidden'>hidden</a>,
    <a href='#text2'>date</a>, 
    <a href='#date-popup'>date-popup</a>, 
    <a href='#text2'>datetime-local</a>, 
    <a href='#text2'>email</a>, 
    <a href='#text2'>number</a>, 
    <a href='#text2'>password</a>, 
    <a href='#phone'>phone</a>, 
    <a href='#radio'>radio</a>, 
    <a href='#radios'>radios</a>,
    <a href='#select'>select</a>,
    <a href='#submit'>submit</a>,
    <a href='#text'>text</a>,
    <a href='#textarea'>textarea</a>,
    <a href='#title'>title</a>
  </div>

  <a name="cols"></a>
  <h3>cols (optional)</h3>
  <div>This key is for the textarea field and sets the number of columns preset to the &lt;textarea> tag.</div>
  <pre>&lt;?php
$form['body'] = array(
  'type' => 'textarea',
  'label' => 'Enter body text',
  'rows' => 5,
  <em>'cols' => 60,</em>
);
?>
  </pre>
  <label for="body">Enter body text: </label><textarea name="body" id="body" class="" rows="5" cols="60"></textarea>
  <div>&nbsp;</div>
  <div>Used in field types: <br/>
    <a href='#textarea'>textarea</a>,
  </div>

  <a name="default_value"></a>
  <h3>default_value (optional)</h3>
  <div>This key is used to set a pre-defined value to the field.</div>
  <pre>&lt;?php
$form['password'] = array(
  'type' => 'text',
  'label' => 'Car number',
  <em>'default_value' => '4A8T68',</em>
);
?>
  </pre>
  <label for="car_number">Car number: </label><input type="text" name="car_number" id="car_number" value="4A8T68" class="" />
  <div>&nbsp;</div>
  <div>Used in field types: <br/>
    <a href='#checkbox'>checkbox</a>,
    <a href='#checkboxes'>checkboxes</a>,
    <a href='#custom'>custom</a>,
    <a href='#hidden'>hidden</a>,
    <a href='#text2'>date</a>, 
    <a href='#date-popup'>date-popup</a>, 
    <a href='#text2'>datetime-local</a>, 
    <a href='#text2'>email</a>, 
    <a href='#text2'>number</a>, 
    <a href='#text2'>password</a>, 
    <a href='#phone'>phone</a>, 
    <a href='#radio'>radio</a>, 
    <a href='#radios'>radios</a>,
    <a href='#select'>select</a>,
    <a href='#text'>text</a>,
    <a href='#textarea'>textarea</a>,
  </div>

  <a name="item_prefix"></a>
  <h3>item_prefix (optional)</h3>
  <div>This key is used to place markup before the field.</div>
  <div>Usually it is used in conjunction with the item_suffix to enter the field (or a group of fields) into a wrapper.</div>
  <pre>&lt;?php
$form['price'] = array(
  'type' => 'text',
  'label' => 'Price',
  <em>'item_prefix' => '&lt;div class="price-wrapper" style="padding:20px;background:#aaa;">',</em>
  'item_suffix' => '&lt;/div>',
);
?>
  </pre>
  <div class="price-wrapper" style="padding:20px;background:#aaa;">
  <label for="price">Car number: </label><input type="text" name="price" id="price" value="" class="" />
  </div>
  <div>&nbsp;</div>
  <div>Used in field types: <br/>
    <a href='#button'>button</a>,
    <a href='#checkbox'>checkbox</a>,
    <a href='#checkboxes'>checkboxes</a>,
    <a href='#custom'>custom</a>,
    <a href='#text2'>date</a>, 
    <a href='#date-popup'>date-popup</a>, 
    <a href='#text2'>datetime-local</a>, 
    <a href='#text2'>email</a>, 
    <a href='#text2'>number</a>, 
    <a href='#text2'>password</a>, 
    <a href='#phone'>phone</a>, 
    <a href='#radio'>radio</a>, 
    <a href='#radios'>radios</a>,
    <a href='#select'>select</a>,
    <a href='#submit'>submit</a>,
    <a href='#text'>text</a>,
    <a href='#textarea'>textarea</a>,
    <a href='#title'>title</a>
  </div>

  <a name="item_suffix"></a>
  <h3>item_suffix (optional)</h3>
  <div>This key is used to place markup after the field.</div>
  <div>Usually it is used in conjunction with the item_prefix to enter the field (or a group of fields) into a wrapper.</div>
  <pre>&lt;?php
$form['price'] = array(
  'type' => 'text',
  'label' => 'Price',
  'item_prefix' => '&lt;div class="price-wrapper" style="padding:20px;background:#aaa;">',
  <em>'item_suffix' => '&lt;/div>',</em>
);
?>
  </pre>
  <div>&nbsp;</div>
  <div>Used in field types: <br/>
    <a href='#button'>button</a>,
    <a href='#checkbox'>checkbox</a>,
    <a href='#checkboxes'>checkboxes</a>,
    <a href='#custom'>custom</a>,
    <a href='#text2'>date</a>, 
    <a href='#date-popup'>date-popup</a>, 
    <a href='#text2'>datetime-local</a>, 
    <a href='#text2'>email</a>, 
    <a href='#text2'>number</a>, 
    <a href='#text2'>password</a>, 
    <a href='#phone'>phone</a>, 
    <a href='#radio'>radio</a>, 
    <a href='#radios'>radios</a>,
    <a href='#select'>select</a>,
    <a href='#submit'>submit</a>,
    <a href='#text'>text</a>,
    <a href='#textarea'>textarea</a>,
    <a href='#title'>title</a>
  </div>

  <a name="label"></a>
  <h3>label</h3>
  <div>This key is mandatory for most field types. It is used as the field &lt;label> tag.</div>
  <div>In checkbox and radio field types, the label is placed after the checkbox/radio.</div>
  <pre>&lt;?php
$form['password'] = array(
  'type' => 'password',
  <em>'label' => 'Enter password',</em>
);
?>
  </pre>
  <div>Used in field types: <br/>
    <a href='#button'>button</a>,
    <a href='#checkbox'>checkbox</a>,
    <a href='#checkboxes'>checkboxes</a>,
    <a href='#custom'>custom</a>,
    <a href='#text2'>date</a>, 
    <a href='#date-popup'>date-popup</a>, 
    <a href='#text2'>datetime-local</a>, 
    <a href='#text2'>email</a>, 
    <a href='#text2'>number</a>, 
    <a href='#text2'>password</a>, 
    <a href='#phone'>phone</a>, 
    <a href='#radio'>radio</a>, 
    <a href='#radios'>radios</a>,
    <a href='#select'>select</a>,
    <a href='#submit'>submit</a>,
    <a href='#text'>text</a>,
    <a href='#textarea'>textarea</a>,
    <a href='#title'>title</a>
  </div>

  <a name="rows"></a>
  <h3>rows (optional)</h3>
  <div>This key is for the textarea field and sets the number of rows preset to the &lt;textarea> tag.</div>
  <pre>&lt;?php
$form['body'] = array(
  'type' => 'textarea',
  'label' => 'Enter body text',
  <em>'rows' => 5,</em>
  'cols' => 60,
);
?>
  </pre>
  <div>Used in field types: <br/>
    <a href='#textarea'>textarea</a>,
  </div>

  <a name="size"></a>
  <h3>size (optional)</h3>
  <div>This key is for text fields and sets the size attribute of the &lt;input> tag.</div>
  <pre>&lt;?php
$form['password'] = array(
  'type' => 'password',
  'label' => 'Enter password',
  <em>'size' => 40,</em>
);
?>
  </pre>
  <div>Used in field types: <br/>
    <a href='#custom'>custom</a>,
    <a href='#text2'>date</a>, 
    <a href='#date-popup'>date-popup</a>, 
    <a href='#text2'>datetime-local</a>, 
    <a href='#text2'>email</a>, 
    <a href='#text2'>number</a>, 
    <a href='#text2'>password</a>, 
    <a href='#phone'>phone</a>, 
    <a href='#text'>text</a>,
  </div>

  <div>&nbsp;</div>
  <div class="top"><a href="#top">Back to top</a></div>
  <hr />
  <div>&nbsp;</div>

  <div class="footer-note">Last update: December 28<sup>th</sup>, 2015 | &copy; All rights reserved, Ami Geva</div>
</div>
