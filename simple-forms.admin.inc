<?php

function simpleforms_site_transient_update_plugins($value) {
  if (is_admin()) {
    // checks latest version of this plugin
    $p = get_plugin_data(dirname(__FILE__) . '/simple-forms.php');
    $p += array(
      'query' => 'simpleforms',
    );
    if (!isset($_SESSION["{$p['query']}_latest_version"]) || !isset($_SESSION["{$p['query']}_latest_version_last_check"]) || (isset($_SESSION["{$p['query']}_latest_version_last_check"]) && $_SESSION["{$p['query']}_latest_version_last_check"] < time() - 900)) { // Checks against the versions server (dmdapi.oryxsignals.com) no more than once every 15 minutes
      $response = wp_remote_get('http://dmdapi.oryxsignals.com/?' . http_build_query(array('p' => $p['query'])));
      $res = (is_array($response) && isset($response['response']['code']) && $response['response']['code'] == 200) ? json_decode($response['body']) : FALSE;
      if (is_object($res) && isset($res->version)) {
        $_SESSION["{$p['query']}_latest_version"] = $res->version;
        $_SESSION["{$p['query']}_latest_version_last_check"] = time();
      }
      else {
        unset($_SESSION["{$p['query']}_latest_version"]);
        unset($_SESSION["{$p['query']}_latest_version_last_check"]);
      }
    }
    $new_version = (isset($_SESSION["{$p['query']}_latest_version"])) ? $_SESSION["{$p['query']}_latest_version"] : FALSE;
    if ($new_version && $new_version != $p['Version']) {
      $value->response['simple-forms/simple-forms.php'] = (object) array(
            'id' => 0,
            'slug' => 'simple-forms',
            'plugin' => 'simple-forms/simple-forms.php',
            'new_version' => $new_version,
            'url' => '',
            'package' => '',
      );
    }
  }

  return $value;
}

add_filter('site_transient_update_plugins', 'simpleforms_site_transient_update_plugins');

function simpleforms_plugin_action_links($links, $file) {
  $this_plugin = 'simple-forms/simple-forms.php';

  if ($file == $this_plugin) {
    $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=simpleforms-settings">Settings</a>';
    $help_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=simpleforms-help">Help</a>';
    $changelog_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=simpleforms-changelog">Change Log</a>';
    array_unshift($links, $settings_link);
    array_push($links, $help_link);
    array_push($links, $changelog_link);
  }

  return $links;
}

add_filter('plugin_action_links', 'simpleforms_plugin_action_links', 10, 2);

function simpleforms_admin() {
  add_options_page(__('Simple Forms Settings'), __('Simple Forms'), 'manage_options', 'simpleforms-settings', '_simpleforms_settings');
  add_submenu_page('simpleforms_settings', 'Simple Forms Help', 'Help', 'manage_options', 'simpleforms-help', '_simpleforms_help');
  add_submenu_page('simpleforms_settings', 'Simple Forms Change Log', 'Change Log', 'manage_options', 'simpleforms-changelog', '_simpleforms_change_log');
}

add_action('admin_menu', 'simpleforms_admin');

function _simpleforms_help() {
  include_once 'simple-forms.help.php';
}

function _simpleforms_change_log() {
  $output = '';
  $file = dirname(__FILE__) . '/CHANGELOG.md';
  if (file_exists($file)) {
    $readme = file_get_contents($file);
    if (class_exists('Parsedown')) {
      $markup = new Parsedown;
      $output .= $markup->text($readme);
    }
    else {
      $output .= '<div class="markdown-wrapper">';
      $output .= '<p><em>NOTE: To properly render this markdown document, install and activate the devel-tools plugin!</em></p>';
      $r = str_replace("\n", '<br />', $readme);
      $r = preg_replace('/###(.*?)###/', '<h3>$1</h3>', $r);
      $r = preg_replace('/##(.*?)##/', '<h2>$1</h2>', $r);
      $r = preg_replace('/#(.*?)#/', '<h1>$1</h1>', $r);
      $r = preg_replace('/\*\*\*(.*?)\*\*\*/', '<strong><em>$1</em></strong>', $r);
      $r = preg_replace('/\*\*(.*?)\*\*/', '<em>$1</em>', $r);
      $output .= $r;
      $output .= '</div>';
    }
  }
  print $output;
}

function _simpleforms_settings() {
  $form = array(
    'id' => 'simpleforms',
    'type' => 'admin_settings',
  );

  $form['help_link'] = array(
    'markup' => '<a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=simpleforms-help">' . __('Simple Forms Help Page') . '</a>',
  );
  $form['date_popup_format'] = array(
    'label' => __('Default date-popup field format'),
    'type' => 'text',
    'default_value' => 'yy-mm-dd',
    'required' => TRUE,
  );
  $form['load_jquery_libs'] = array(
    'label' => __('Load jquery libraries and css (site)'),
    'type' => 'checkbox',
    'default_value' => FALSE,
  );
  $form['load_jquery_libs_source'] = array(
    'label' => __('jQuery libraries and css source'),
    'type' => 'radios',
    'options' => array(
      'local' => __('Local'),
      'google' => __('Google CDN'),
    ),
    'default_value' => 'google',
  );
  $form['load_jquery_libs_admin'] = array(
    'label' => __('Load jquery libraries and css (admin pages)'),
    'type' => 'checkbox',
    'default_value' => TRUE,
  );
  $form['use_preloader'] = array(
    'label' => __('Use preloader on forms save (disabled on admin pages)'),
    'type' => 'checkbox',
    'default_value' => FALSE,
    'attributes' => array(
      'onclick' => 'hideShow(this.id, [\'preloader_type\',\'preloader_text\',\'preloader_markup\']);',
    ),
    'suffix' => '<script type=\'text/javascript\'>/* <![CDATA[ */ jQuery(document).ready(function() {hideShow(\'use_preloader\', [\'preloader_type\',\'preloader_text\',\'preloader_markup\']);}); /* ]]> */</script>',
  );
  $form['form_exclude_include'] = array(
    'label' => __('Exclude/include IDs'),
    'type' => 'radios',
    'options' => array(
      'exclude' => __('Exclude list'),
      'include' => __('Include list'),
    ),
    'default_value' => 'exclude',
  );
  $form['forms_id_list'] = array(
    'label' => __('Form IDs list'),
    'type' => 'textarea',
    'rows' => 5,
    'cols' => 60,
    'description' => __('List of form IDs, one per line, that will be excluded/included for the preloader functionality.'),
  );
  $form['preloader_type'] = array(
    'label' => __('Preloader type (1-7)'),
    'size' => '5',
    'default_value' => '1',
    'type' => 'text',
    'description' => __('Select one from these possible types:') . '<div>&nbsp;</div>' .
    '<div class="fl label"><big>1</big></div>' .
    '<div class="fl spinner1"><div class="double-bounce1"></div><div class="double-bounce2"></div></div>' .
    '<div class="fl label"><big>2</big></div>' .
    '<div class="fl spinner2"><div class="spinner-container container1"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container2"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container3"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div></div>' .
    '<div class="fl label"><big>3</big></div>' .
    '<div class="fl spinner3"><div class="dot1"></div><div class="dot2"></div></div>' .
    '<div class="fl label"><big>4</big></div>' .
    '<div class="fl spinner4"></div>' .
    '<div class="fl label"><big>5</big></div>' .
    '<div class="fl spinner5"><div class="cube1"></div><div class="cube2"></div></div>' .
    '<div class="fl label"><big>6</big></div>' .
    '<div class="fl spinner6"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>' .
    '<div class="fl label"><big>7</big></div>' .
    '<div class="fl spinner7"><div class="circ1"></div><div class="circ2"></div><div class="circ3"></div><div class="circ4"></div></div>',
  );
  $form['preloader_text'] = array(
    'label' => __('Preloader text'),
    'size' => '20',
    'default_value' => __('Please wait ...', 'simple-forms'),
    'type' => 'text',
  );
  $form['preloader_markup'] = array(
    'markup' => '<hr width="80%" />',
  );

  $admin_form = new simple_form($form);

  print ($admin_form->render_form());
}
