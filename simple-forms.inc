<?php

/* * ****************************************
 *             Initialization               *
 * **************************************** */

function simpleforms_init() {
  $lang = get_locale();
  load_plugin_textdomain('simple-forms', FALSE, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('init', 'simpleforms_init');

/* * ****************************************
 *           Scripts and Styles             *
 * **************************************** */
$fidList = array();

function simpleforms_all_scriptsandstyles() {
  $simple_forms_js_params = _simpleforms_js_params();

  $jquery_ui_js = (get_option('simpleforms_load_jquery_libs_source', 'google') == 'google') ? '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js' : plugin_dir_url(__FILE__) . '/js/jquery-ui.min.js';
  wp_register_script('jquery_ui_core', $jquery_ui_js, array('jquery'), '1.11.2', FALSE);
  wp_register_script('simple_forms', plugins_url('/simple-forms/js/simpleforms.js', 'simple-forms'), array('jquery'), SIMPLEFORMS_VERSION, TRUE);
  $jquery_ui_css = (get_option('simpleforms_load_jquery_libs_source', 'google') == 'google') ? '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css' : plugin_dir_url(__FILE__) . '/css/jquery-ui.css';
  wp_register_style('jquery_ui_core_css', $jquery_ui_css, array(), '1.11.2', 'all');
  if (get_option('simpleforms_load_jquery_libs', 0)) {
    wp_enqueue_script('jquery_ui_core');
    wp_enqueue_style('jquery_ui_core_css');
  }
  wp_enqueue_script('simple_forms');
  if (get_option('simpleforms_use_preloader', 0)) {
    wp_enqueue_style('dmdlanding-fakeLoader-css', plugin_dir_url(__FILE__) . 'css/fakeLoader.css', array(), SIMPLEFORMS_VERSION, 'all');
    wp_enqueue_style('simple_forms_css', plugins_url('/simple-forms/css/simple-forms.css', 'simple-forms'), array(), SIMPLEFORMS_VERSION, 'all');
  }
  wp_localize_script('simple_forms', 'simpleFormsSettings', $simple_forms_js_params);
}

add_action('wp_enqueue_scripts', 'simpleforms_all_scriptsandstyles');

function simpleforms_admin_scriptsandstyles() {
  $simple_forms_js_params = _simpleforms_js_params();

  $jquery_ui_js = (get_option('simpleforms_load_jquery_libs_source', 'google') == 'google') ? '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js' : plugin_dir_url(__FILE__) . '/js/jquery-ui.min.js';
  wp_register_script('jquery_ui_core', $jquery_ui_js, array('jquery'), '1.11.2', FALSE);
  wp_register_script('simple_forms', plugins_url('/simple-forms/js/simpleforms.js', 'simple-forms'), array('jquery'), SIMPLEFORMS_VERSION, TRUE);
  $jquery_ui = (get_option('simpleforms_load_jquery_libs_source', 'google') == 'google') ? '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css' : plugin_dir_url(__FILE__) . '/css/jquery-ui.css';
  wp_register_style('jquery_ui_core_css', $jquery_ui, array(), '1.11.2', 'screen');
  wp_register_style('simple_forms_css', plugins_url('/simple-forms/css/simple-forms.css', 'simple-forms'), array(), SIMPLEFORMS_VERSION, 'screen');
  if (get_option('simpleforms_load_jquery_libs_admin', 1)) {
    wp_enqueue_script('jquery_ui_core');
    wp_enqueue_style('jquery_ui_core_css');
  }
  wp_enqueue_script('simple_forms');
  wp_enqueue_style('simple_forms_css');
  wp_enqueue_style('dmdlanding-fakeLoader', plugins_url('/simple-forms/css/fakeLoader.css', 'simple-forms'), array(), SIMPLEFORMS_VERSION, 'screen');
  wp_enqueue_style('dmdlanding-admin', plugins_url('/simple-forms/css/admin.css', 'simple-forms'), array(), SIMPLEFORMS_VERSION, 'screen');
  wp_localize_script('simple_forms', 'simpleFormsSettings', $simple_forms_js_params);
}

add_action('admin_enqueue_scripts', 'simpleforms_admin_scriptsandstyles');

/* * ****************************************
 *           General Functions              *
 * **************************************** */

function _array2keyvalues($array = array()) {
  $string = '';
  if (!empty($array)) {
    foreach ($array as $key => $value) {
      $string .= "{$key}=\"{$value}\" ";
    }
  }
  $string = trim($string);

  return $string;
}

function _simpleforms_trim_array_values(&$array = array()) {
  if (!empty($array)) {
    foreach ($array as &$val) {
      $val = trim($val);
    }
  }
}

function _simpleforms_js_params() {
  $fl = explode("\n", get_option('simpleforms_forms_id_list', ''));
  _simpleforms_trim_array_values($fl);
  return array(
    'preloader' => array(
      'usePreloader' => get_option('simpleforms_use_preloader', 0),
      'type' => get_option('simpleforms_preloader_type', 1),
      'text' => __(get_option('simpleforms_preloader_text', 'Please wait ...'), 'simple-forms'),
      'exclude' => get_option('simpleforms_form_exclude_include', 'exclude'),
      'fidList' => $fl,
    ),
  );
}
