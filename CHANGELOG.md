<style type="text/css">ul {list-style:square;margin:0 0 0 20px;}ul.wp-submenu {margin:0;}</style>
# Change Log #

### Version: 1.1.10 ###
* Move scripts to footer (1.1.10).

### Version: 1.1.9 ###
* Change default form enctype to ***application/x-www-form-urlencoded*** and add enctype as a form attribute (1.1.9).
* Change token validation to use wp nonce functions (1.1.9).

### Version: 1.1.8 ###
* Add support for form attributes (1.1.8).

### Version: 1.1.7 ###
* Changed ***presubmit_callback*** and ***postsubmit_callback*** to work with arrays instead of strings (1.1.7).
* Bug fix: change location for the apply_filter call to allow alteration of the submit callbacks (1.1.7).

### Version: 1.1.6 ###
* Add hook (filter) to allow plugins to alter a specific form ID. (1.1.6).

### Version: 1.1.5 ###
* Bug fix: preloader trys to load even when set as disabled. The value "0" is passed as true. (1.1.5).

### Version: 1.1.4 ###
* Change the return values method of ***if ($(this).valid())*** check. (1.1.4).
* Initiate the preloader inside the submit event in the ***doFormValidation*** function. (1.1.4).
* Add more languages (partial): Arabic, German, and Russian. (1.1.4).

### Version: 1.1.3 ###
* Add languages domain to support string translation and the ***languages*** folder with Chinese (partial) translation (1.1.3).

### Version: 1.1.2 ###
* Implement form ***prefix*** and ***suffix*** (1.1.2).

### Version: 1.1.1 ###
* Bug fix: remove loader when form inside iframe didn't work (1.1.1).

### Version: 1.1.0 ###
* Check plugin version only on the admin pages.
* Add option to choose the source of the jQuery files: local or google CDN (1.0.26).
* Add option to run the preloader from within an iframe on the parent document. If a form has the ***inside-iframe*** class the ***doPreloader*** will be called with the iframe variable set to true (1.0.25).
* Bug fix: Changed where the ***form_fields*** array is initialized in the form class (1.0.24).
* Bug fix: field type ***checkboxes*** does not save (1.0.23).
* Add two new parameters to the form: ***presubmit_callback*** and ***postsubmit_callback***. These parameters define custom callbacks to form submit, pre and post the default form submit function (1.0.22).
* Display this _Version History_ document (1.0.21).
* Add ***item_prefix*** and ***item_suffix*** to the ***&lt;tr>*** element when rendering an ***admin_settings*** form type (1.0.21).
* Add field type ***custom*** and parameter ***theme_callback*** to allow a custom callback function for rendering a specific field (1.0.20). 
* Add class to the ***&lt;tr>*** element in the ***admin_settings*** form (1.0.18).
* Fixed behavior of form save operation when a field attribute is ***readonly*** and/or ***disabled*** (1.0.18).
* Start working on the [help](/wp-admin/admin.php?page=simpleforms-help) document (1.0.17).
* Changed the admin menu presence from a seperate menu to be part of the wordpress settings menu (1.0.17).
* Add the parameters ***item_prefix_*** and ***item_suffix*** to thye field (1.0.16).
* Add output for regular forms (1.0.16).
* Various bugs fix.

### Version: 1.0.15 ###
* ???
