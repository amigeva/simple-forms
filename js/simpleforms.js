(function($) {
  $(document).ready(function() {
    // Implement datepicker
    if ($.isFunction($.fn.datepicker)) {
      var date = new Date();
      var year = date.getFullYear();
      var month = ("0" + (date.getMonth() + 1)).slice(-2);
      var day = ("0" + date.getDate()).slice(-2);
      $("input.date-popup").datepicker({
        dateFormat: $("input.date-popup").attr('format'),
        changeMonth: true,
        changeYear: true,
        yearRange: $("input.date-popup").attr('year-range')
      });
    }

    // Implement preloader
    if (simpleFormsSettings.preloader.usePreloader !== '0') {
      $("form.do-preloader").submit(function(e) {
        iframe = ($(this).hasClass('inside-iframe')) ? true : false;
        if ($.inArray($(this).attr('id'), simpleFormsSettings.preloader.fidList) !== -1 && simpleFormsSettings.preloader.exclude === 'include') {
          doLoader(simpleFormsSettings.preloader.type, iframe);
        }
        else if ($.inArray($(this).attr('id'), simpleFormsSettings.preloader.fidList) === -1 && simpleFormsSettings.preloader.exclude === 'exclude') {
          doLoader(simpleFormsSettings.preloader.type, iframe);
        }
        return true;
      });
    }
  });
})(jQuery);

function doFormValidation(formId, formFields, submitHandler) {
  var $ = jQuery;
  $(document).on('submit', '#' + formId, function() {
    iframe = ($(this).hasClass('inside-iframe')) ? true : false;
    if ($.inArray($(this).attr('id'), simpleFormsSettings.preloader.fidList) !== -1 && simpleFormsSettings.preloader.exclude === 'include') {
      doLoader(simpleFormsSettings.preloader.type, iframe);
    }
    else if ($.inArray($(this).attr('id'), simpleFormsSettings.preloader.fidList) === -1 && simpleFormsSettings.preloader.exclude === 'exclude') {
      doLoader(simpleFormsSettings.preloader.type, iframe);
    }
    var form = $('#' + formId);
    var validateThis = {};
    var notValidMessage = {};
    if (typeof formFields !== 'undefined') {
      // validation rules and messages recieved as an object: formFields
      $.each(formFields, function(i, v) {
        validateThis[i] = v.rules;
        notValidMessage[i] = v.messages;
      });
    }
    else {
      // validation rules and messages are guessed from the actual form
      var $inputs = $('#' + formId + ' :input');
      $inputs.each(function() {
        validateThis[this.name] = {
          required: (this.className.indexOf('required') !== -1) ? true : false +
                  JSON.parse($(this).attr('validate-rules'))
        };
        notValidMessage[this.name] = {
          required: '<strong>' + this.parentElement.outerText.replace(/[\*]+?[\:]+?/, '') + '</strong> is required.',
          digits: '<strong>' + this.parentElement.outerText.replace(/[\*]+?[\:]+?/, '') + '</strong> should only include digits.'
        };
      });
    }
    form.validate({
      ignore: '.ignore',
      onfocusout: true,
      focusInvalid: true,
      rules: validateThis,
      messages: notValidMessage,
      invalidHandler: function(event, validator) {
        iframe = ($(this).hasClass('inside-iframe')) ? true : false;
        removeLoader(iframe);
      },
      errorLabelContainer: $('#' + formId + ' div.form-errors-wrapper')
    });
    if ($(this).valid()) {
      if (typeof submitHandler !== 'undefined') {
        return window[submitHandler](form);
      }
      else {
        return true;
      }
    }
    else {
      iframe = $('#' + formId).hasClass('inside-iframe');
      removeLoader(iframe);
      return false;
    }
  });
}

function hideShow(f, e) {
  var $ = jQuery;
  e.forEach(function(element) {
    if ($('#' + f).is(':checked')) {
      $('.' + element).slideDown('slow');
    }
    else {
      $('.' + element).slideUp('slow');
    }
  });
}

function hideShowByRadios(e1, e2) {
  var $ = jQuery;
  var radio = $('input[name=' + e1 + ']:checked').val();
  $.each(e2, function(i, v) {
    if (v === e2[radio]) {
      $(v).show('fast');
    }
    else {
      $(v).hide('fast');
    }
  });
}

function hideShowArray(f, e) {
  var $ = jQuery;
  var radios = $('input[name=' + f + ']');
  radios.each(function(index, value) {
    if ($(this).is(':checked')) {
      e[index].forEach(function(el, i) {
        $('.' + el).show('fast');
      });
      e.forEach(function(el, i) {
        if (i !== index) {
          e[i].forEach(function(b) {
            $('.' + b).hide('fast');
          });

        }
      });
    }
  });
}

function doLoader(type, iframe) {
  var $ = jQuery;
  iframe = (typeof iframe !== 'undefined') ? iframe : false;
  var spinner = 'spinner1';
  var preloader = '<div class="double-bounce1"></div><div class="double-bounce2">';
  switch (type) {
    case 2:
    case '2':
      spinner = 'spinner2';
      preloader = '<div class="spinner-container container1"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container2"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container3"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div>';
      break;
    case 3:
    case '3':
      spinner = 'spinner3';
      preloader = '<div class="dot1"></div><div class="dot2"></div>';
      break;
    case 4:
    case '4':
      spinner = 'spinner4';
      preloader = '';
      break;
    case 5:
    case '5':
      spinner = 'spinner5';
      preloader = '<div class="cube1"></div><div class="cube2"></div>';
      break;
    case 6:
    case '6':
      spinner = 'spinner6';
      preloader = '<div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div>';
      break;
    case 7:
    case '7':
      spinner = 'spinner7';
      preloader = '<div class="circ1"></div><div class="circ2"></div><div class="circ3"></div><div class="circ4"></div>';
      break;
  }
  var d = '<div id="fakeLoader" style="display:none;"><div class="fl-text">' + simpleFormsSettings.preloader.text + '</div><div class="fl ' + spinner + '">' + preloader + '</div></div>';
  var b = (iframe) ? $(window.parent.document.body) : $("body");
  if (!b.find("#fakeLoader")) {
    b.prepend(d);
  }
  else {
    b.find("#fakeLoader").remove();
    b.prepend(d);
  }
  var winW = (iframe) ? $(window.parent).width() : $(window).width();
  var winH = (iframe) ? $(window.parent).height() : $(window).height();
  b.find("#fakeLoader .fl").css({
    top: winH / 2
  });
  b.find("#fakeLoader").css({
    width: winW,
    height: winH,
    opacity: 1
  }).show();
}

function removeLoader(iframe) {
  var $ = jQuery;
  iframe = (typeof iframe !== 'undefined') ? iframe : false;
  var b = (iframe) ? $(window.parent.document.body) : $("body");
  b.find("#fakeLoader").remove();
}
