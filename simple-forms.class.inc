<?php

class simple_form {

  private $form_fields;

  public function __construct($form = array()) {
    $this->init($form);
  }

  public function init($form) {
    $this->form = $form;
    $this->form_fields = array('id', 'class', 'action', 'method', 'enctype', 'attributes', 'redirect', 'prefix', 'suffix', 'type', 'colon', 'op', 'valid', 'invalid_fields', 'invalid_other', 'presubmit_callback', 'postsubmit_callback');
    $this->form['id'] = (isset($this->form['id']) && !empty($this->form['id'])) ? $this->form['id'] : 'form-' . time();
    $nonce = wp_create_nonce("form-{$this->form['id']}");
    if ((isset($this->form['token']) && $this->form['token']) || (isset($this->form['type']) && $this->form['type'] == 'admin_settings')) {
      $this->form['token'] = array(
        'type' => 'hidden',
        'value' => $nonce,
      );
    }

    // Add support for form_ID_alter hook
    $fid = str_replace('-', '_', $this->form['id']);
    $this->form = apply_filters("form_{$fid}_alter", $this->form);
  }

  private function init_form() {
    $this->form['class'] = (isset($this->form['class']) && !empty($this->form['class'])) ? $this->form['class'] : '';
    $this->form['prefix'] = (isset($this->form['prefix']) && !empty($this->form['prefix'])) ? $this->form['prefix'] : '';
    $this->form['suffix'] = (isset($this->form['suffix']) && !empty($this->form['suffix'])) ? $this->form['suffix'] : '';
    $this->form['action'] = (isset($this->form['action']) && !empty($this->form['action'])) ? $this->form['action'] : $this->language_uri($_SERVER['REQUEST_URI']);
    $this->form['method'] = (isset($this->form['method']) && !empty($this->form['method'])) ? $this->form['method'] : 'POST';
    $this->form['enctype'] = (isset($this->form['enctype']) && !empty($this->form['enctype'])) ? $this->form['enctype'] : 'application/x-www-form-urlencoded';
    $this->form['attributes'] = (isset($this->form['attributes'])) ? $this->form['attributes'] : NULL;
    $this->form['redirect'] = (isset($this->form['redirect']) && !empty($this->form['redirect'])) ? $this->form['redirect'] : $this->language_uri($_SERVER['REQUEST_URI']);
    $this->form['colon'] = (isset($this->form['colon'])) ? $this->form['colon'] : TRUE;

    if (isset($this->form['type']) && $this->form['type'] == 'admin_settings') {
      // Prepare the 'admin_settings' form
      foreach ($this->form as $name => &$field) {
        if (!in_array($name, $this->form_fields)) {
          $field['default_value'] = (isset($field['default_value'])) ? $field['default_value'] : NULL;
          $field['type'] = (isset($field['type'])) ? $field['type'] : 'markup';
          if (isset($this->form['invalid_fields']) && is_array($this->form['invalid_fields']) && key_exists($name, $this->form['invalid_fields'])) {
            $field['default_value'] = filter_input(INPUT_POST, $name);
            $field['class'][] = 'error-field';
          }
          else {
            $field['default_value'] = get_option("{$this->form['id']}_{$name}", $field['default_value']);
          }
        }
      }
    }
    else {
      // Prepare the normal form
      foreach ($this->form as $name => &$field) {
        if (!in_array($name, $this->form_fields)) {
          $field['default_value'] = (isset($field['default_value'])) ? $field['default_value'] : NULL;
          $field['type'] = (isset($field['type'])) ? $field['type'] : 'markup';
          if (isset($field['required']) && $field['required']) {
            $field['class'][] = 'required';
          }
        }
      }
    }
  }

  public function render_form() {

    $output = '';

    $this->submit_form();

    $this->init_form();

    $classes = (isset($this->form['class']) && is_array($this->form['class'])) ? implode(' ', $this->form['class']) : $this->form['class'];

    $attributes = (isset($this->form['attributes'])) ? $this->form['attributes'] : NULL;
    $form_attributes = '';
    if (is_array($attributes)) {
      foreach ($attributes as $key => $attribute) {
        $form_attributes .= "{$key}=\"{$attribute}\" ";
      }
    }
    else {
      $form_attributes .= $attributes;
    }

    if ((isset($this->form['invalid_fields']) && !empty($this->form['invalid_fields'])) || (isset($this->form['invalid_other']) && !empty($this->form['invalid_other']))) {
      // If we have validation errors, we display them
      $output .= '<div id="message" class="error"><ul>';
      if (is_array($this->form['invalid_fields'])) {
        foreach ($this->form['invalid_fields'] as $invalid_field) {
          $error_message = sprintf(__('The field %1$s is required'), '<em>' . $invalid_field['label'] . '</em>');
          $output .= "<li>{$error_message}</li>";
        }
      }
      if (is_array($this->form['invalid_other'])) {
        foreach ($this->form['invalid_other'] as $invalid_other) {
          $output .= "<li>{$invalid_other}</li>";
        }
      }
      $output .= '</ul></div>';
      unset($this->form['invalid_fields']);
    }
    else if (isset($this->form['valid']) && $this->form['valid']) {
      // no error so we guess the form is submited successfully
      $output .= '<div id="message" class="updated fade">';
      $output .= __('Form was submited successfully');
      $output .= '</div>';
    }

    $preloader = (get_option('simpleforms_use_preloader', 0) && (!isset($this->form['type']) || $this->form['type'] != 'admin_settings')) ? 'do-preloader' : '';

    // Start building the form
    $output .= $this->form['prefix'] . '<form id="' . $this->form['id'] . '" action="' . $this->form['action'] . '" method="' . $this->form['method'] . '" class="form-validate ' . $classes . ' ' . $preloader . '" enctype="' . $this->form['enctype'] . '" ' . $form_attributes . '>';

    $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<table class="form-table"><tbody>' : '';

    $hidden_fields = '';
    foreach ($this->form as $name => $field) {
      if (!in_array($name, $this->form_fields)) {
        $maxlength = (isset($field['maxlength'])) ? "maxlength=\"{$field['maxlength']}\"" : '';
        $size = (isset($field['size'])) ? "size=\"{$field['size']}\"" : '';
        $cols = (isset($field['cols'])) ? "cols=\"{$field['cols']}\"" : '';
        $rows = (isset($field['rows'])) ? "rows=\"{$field['rows']}\"" : '';
        $prefix = (isset($field['prefix'])) ? "<span class=\"field-prefix\">{$field['prefix']}</span>" : '';
        $suffix = (isset($field['suffix'])) ? "<span class=\"field-suffix\">{$field['suffix']}</span>" : '';
        $item_prefix = (isset($field['item_prefix'])) ? $field['item_prefix'] : '';
        $item_suffix = (isset($field['item_suffix'])) ? $field['item_suffix'] : '';
        $placeholder = (isset($field['placeholder'])) ? "placeholder=\"{$field['placeholder']}\"" : '';
        $class = (isset($field['class'])) ? $field['class'] : NULL;
        $field_class = (is_array($class)) ? implode(' ', $class) : $class;
        $required = (isset($field['required']) && $field['required']) ? '<span class="required">*</span>' : '';
        $attributes = (isset($field['attributes'])) ? $field['attributes'] : NULL;
        $field_attributes = '';
        if (is_array($attributes)) {
          foreach ($attributes as $key => $attribute) {
            $field_attributes .= "{$key}=\"{$attribute}\" ";
          }
        }
        else {
          $field_attributes .= $attributes;
        }
        $colon = ($this->form['colon']) ? ':' : '';
        $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? "{$item_prefix}<tr class=\"form-item form-item-{$name} {$name} {$field_class}\">" : $item_prefix;
        switch ($field['type']) {
          case 'custom':
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<th scope="row">' : "<div class=\"form-item form-item-{$name} {$name}\">";
            $output .= "<label for=\"{$name}\">{$field['label']}{$required}{$colon} </label>";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</th><td>' : '';

            $field['name'] = $name;
            $output .= $prefix . $field['theme_callback']($field) . $suffix;

            $output .= (isset($field['description']) && !empty($field['description'])) ? "<p><em>{$field['description']}</em></p>" : '';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
          case 'number': // HTML5
          case 'date': // HTML5
          case 'datetime-local': // HTML5
          case 'password':
          case 'email': // HTML5
          case 'text':
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<th scope="row">' : "<div class=\"form-item form-item-{$name} {$name}\">";
            $output .= "<label for=\"{$name}\">{$field['label']}{$required}{$colon} </label>";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</th><td>' : '';
            $output .= "{$prefix}<input type=\"{$field['type']}\" name=\"{$name}\" id=\"{$name}\" value=\"{$field['default_value']}\" class=\"{$field_class}\" {$field_attributes} {$size} {$maxlength} {$placeholder}/>{$suffix}";
            $output .= (isset($field['description']) && !empty($field['description'])) ? "<p><em>{$field['description']}</em></p>" : '';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
          case 'hidden':
            $hidden_fields .= "<input type=\"{$field['type']}\" name=\"{$name}\" value=\"{$field['value']}\" />";
            break;
          case 'phone':
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<th scope="row">' : "<div class=\"form-item form-item-{$name} {$name}\">";
            $output .= "<label for=\"{$name}\">{$field['label']}{$required}{$colon} </label>";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</th><td>' : '';
            if (isset($field['country_code']) && $field['country_code']) {
              $ph = __('Country code', 'simple-forms');
              $output .= "{$prefix}<input type=\"text\" name=\"{$name}-code\" id=\"{$name}-code\" class=\"area-code {$field_class}\" value=\"\" placeholder=\"{$ph}\" readonly />{$suffix}";
            }
            $output .= "{$prefix}<input type=\"{$field['type']}\" name=\"{$name}\" id=\"{$name}\" value=\"{$field['default_value']}\" class=\"{$field_class}\" {$field_attributes} {$size} {$maxlength} {$placeholder}/>{$suffix}";
            $output .= (isset($field['description']) && !empty($field['description'])) ? "<p><em>{$field['description']}</em></p>" : '';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
          case 'date-popup':
            $format = (isset($field['format'])) ? "format=\"{$field['format']}\"" : 'format="' . get_option('simpleforms_date_popup_format', 'yy-mm-dd') . '"';
            $year_range = (isset($field['range'])) ? "year-range=\"{$field['range']}\"" : 'year-range="' . date('Y', time() - (10 * 31536000)) . ':' . date('Y', time() + (10 * 31536000)) . '"';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<th scope="row">' : "<div class=\"form-item form-item-{$name} {$name}\">";
            $output .= "<label for=\"{$name}\">{$field['label']}{$required}{$colon} </label>";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</th><td>' : '';
            $output .= "{$prefix}<input type=\"text\" name=\"{$name}\" id=\"{$name}\" value=\"{$field['default_value']}\" class=\"date-popup {$field_class}\" {$field_attributes} {$format} {$year_range} {$size} {$maxlength} {$placeholder} autocomplete=\"off\" />{$suffix}";
            $output .= (isset($field['description']) && !empty($field['description'])) ? "<p><em>{$field['description']}</em></p>" : '';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
          case 'checkbox':
          case 'radio':
            $selected = ($field['default_value']) ? 'checked' : '';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<th scope="row">' : "<div class=\"form-item form-item-{$name}\">";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</th><td>' : '';
            $output .= "{$prefix}<input type=\"{$field['type']}\" name=\"{$name}\" id=\"{$name}\" value=\"{$field['default_value']}\" class=\"{$field_class}\" {$field_attributes} {$selected}/>{$suffix}";
            $output .= "&nbsp;<label for=\"{$name}\">{$field['label']} </label>&nbsp;";
            $output .= (isset($field['description']) && !empty($field['description'])) ? "<p><em>{$field['description']}</em></p>" : '';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
          case 'textarea':
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<th scope="row">' : "<div class=\"form-item form-item-{$name}\">";
            $output .= "<label for=\"{$name}\">{$field['label']}{$required}{$colon} </label>";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</th><td>' : '';
            $output .= "{$prefix}<textarea name=\"{$name}\" id=\"{$name}\" class=\"{$field_class}\" {$field_attributes} {$cols} {$rows}/>{$field['default_value']}</textarea>{$suffix}";
            $output .= (isset($field['description']) && !empty($field['description'])) ? "<p><em>{$field['description']}</em></p>" : '';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
          case 'checkboxes':
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<th scope="row">' : "<div class=\"form-item form-item-{$name}\">";
            $output .= "<label for=\"{$name}\">{$field['label']}: </label>";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</th><td>' : '';
            $output .= "{$prefix}";
            $i = 0;
            foreach ($field['options'] as $key => $option) {
              if (is_array($field['default_value'])) {
                $selected = '';
                foreach ($field['default_value'] as $k => $v) {
                  $selected = ($v == $key) ? 'checked' : $selected;
                }
              }
              else {
                $selected = ($field['default_value'] == $key) ? 'checked' : '';
              }
              $output .= "&nbsp;<input type=\"checkbox\" name=\"{$name}_{$i}\" id=\"{$name}-{$key}\" value=\"{$key}\" class=\"{$field_class}\" {$field_attributes} {$selected} />&nbsp;{$option}";
              $i++;
            }
            $output .= "{$suffix}";
            $output .= (isset($field['description']) && !empty($field['description'])) ? "<p><em>{$field['description']}</em></p>" : '';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
          case 'radios':
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<th scope="row">' : "<div class=\"form-item form-item-{$name}\">";
            $output .= "<label for=\"{$name}\">{$field['label']}: </label>";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</th><td>' : '';
            $output .= "{$prefix}";
            foreach ($field['options'] as $key => $option) {
              $selected = ($field['default_value'] == $key) ? 'checked' : '';
              $output .= "&nbsp;<input type=\"radio\" name=\"{$name}\" id=\"{$name}-{$key}\" value=\"{$key}\" class=\"{$field_class}\" {$field_attributes} {$selected} />&nbsp;{$option}";
            }
            $output .= "{$suffix}";
            $output .= (isset($field['description']) && !empty($field['description'])) ? "<p><em>{$field['description']}</em></p>" : '';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
          case 'select':
            $multiple = (isset($field['multiple']) && $field['multiple']) ? 'multiple' : '';
            $size = ($multiple && isset($field['size'])) ? "size=\"{$field['size']}\"" : '';
            $name = ($multiple) ? "{$name}[]" : $name;
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<th scope="row">' : "<div class=\"form-item form-item-{$name}\">";
            $output .= "<label for=\"{$name}\">{$field['label']}{$required}{$colon} </label>";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</th><td>' : '';
            $output .= "{$prefix}<select id=\"{$name}\" name=\"{$name}\" class=\"{$field_class}\" {$field_attributes} {$multiple} {$size}>";
            if ($multiple && isset($field['options']) && is_array($field['options'])) {
              $i = 0;
              foreach ($field['options'] as $key => $option) {
                if (is_array($field['default_value'])) {
                  $selected = '';
                  foreach ($field['default_value'] as $k => $v) {
                    $selected = ($v == $key) ? 'selected' : $selected;
                  }
                }
                else {
                  $selected = ($field['default_value'] == $key) ? 'selected' : '';
                }
                if (is_array($option)) {
                  $option_attributes = _array2keyvalues($option);
                  $output .= "<option value=\"{$option['value']}\" {$selected} {$option_attributes}>{$option['option']}</option>";
                }
                else {
                  $output .= "<option value=\"{$key}\" {$selected}>{$option}</option>";
                }
                $i++;
              }
            }
            else if (isset($field['options']) && is_array($field['options'])) {
              foreach ($field['options'] as $key => $option) {
                $selected = ($field['default_value'] == $key) ? 'selected' : '';
                if (is_array($option)) {
                  $option_attributes = _array2keyvalues($option);
                  $output .= "<option value=\"{$option['value']}\" {$selected} {$option_attributes}>{$option['option']}</option>";
                }
                else {
                  $output .= "<option value=\"{$key}\" {$selected}>{$option}</option>";
                }
              }
            }
            $output .= "</select>{$suffix}";
            $output .= (isset($field['description']) && !empty($field['description'])) ? "<p><em>{$field['description']}</em></p>" : '';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
          case 'title':
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<td colspan="2">' : "<div class=\"form-item form-item-{$name}\">";
            $output .= "{$suffix}<h2 class=\"{$field_class}\" {$field_attributes}>{$field['label']}</h2>{$prefix}";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
          case 'button':
          case 'submit':
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<td colspan="2" class="form-action">' : "<div class=\"form-action form-item-{$name}\">";
            $output .= "{$suffix}<input type=\"{$field['type']}\" name=\"{$name}\" id=\"{$name}\" value=\"{$field['value']}\" class=\"{$field_class}\" {$field_attributes} />{$prefix}";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
          case 'markup':
          default:
            $field['markup'] = (isset($field['markup'])) ? $field['markup'] : '';
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<td colspan="2">' : "<div class=\"form-item form-item-{$name}\">";
            $output .= "{$suffix}{$field['markup']}{$prefix}";
            $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</td>' : '</div>';
            break;
        }
        $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? "</tr>{$item_suffix}" : $item_suffix;
      }
    }

    $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '</tbody></table>' : '';

    $output .= '<div class="form-errors-wrapper"></div>';

    $output .= (!empty($hidden_fields)) ? "<div class=\"hidden-fields-wrapper\">{$hidden_fields}</div>" : '';

    $output .= (isset($this->form['type']) && $this->form['type'] == 'admin_settings') ? '<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="' . __('Save settings') . '"></p>' : '';

    $output .= '</form>' . $this->form['suffix'];

    return $output;
  }

  private function validate_form() {
    // TODO: Add support for custom validate callback function
    $this->form['valid'] = TRUE;
    $validation = array();

    // check token (nonce)
    $nonce = (isset($this->form['token']['value'])) ? $this->form['token']['value'] : '';
    if ($this->form['token'] && !wp_verify_nonce($nonce, "form-{$this->form['id']}")) {
      $this->form['valid'] = FALSE;
      $validation['other'] = array(
        'token' => __('Cannot submit form due to invalid token. Reload page and resubmit.'),
      );
    }

    // check required fields
    $fields = array();
    foreach ($this->form as $name => &$field) {
      if (!in_array($name, $this->form_fields)) {
        $field['type'] = (isset($field['type'])) ? $field['type'] : 'markup';
        if (!in_array($field['type'], array('markup'))) {
          if (isset($field['required']) && $field['required'] && empty($field['default_value']) && $field['default_value'] !== '0') {
            $this->form['valid'] = FALSE;
            $fields[$name] = array(
              'name' => $name,
              'label' => (isset($field['label']) && $field['label']) ? $field['label'] : $name,
            );
          }
        }
      }
    }

    $validation['fields'] = $fields;

    return $validation;
  }

  private function submit_form() {
    if ($this->op = filter_input(INPUT_POST, 'submit')) {
      // Form was submitted
      foreach ($this->form as $name => &$field) {
        if (!in_array($name, $this->form_fields)) {
          $field['default_value'] = (isset($field['default_value'])) ? $field['default_value'] : NULL;
          $field['type'] = (isset($field['type'])) ? $field['type'] : 'markup';
          if ($name == 'use_api_payment_method') {
            $a = $field;
          }
          if (!in_array($field['type'], array('markup', 'radio', 'checkbox', 'checkboxes', 'select'))) {
            $field['default_value'] = (!isset($field['attributes']['readonly']) && !isset($field['attributes']['disabled'])) ? htmlspecialchars(filter_input(INPUT_POST, $name)) : $field['default_value'];
          }
          else if (in_array($field['type'], array('select'))) {
            $sel = (isset($field['multiple']) && $field['multiple']) ? filter_input(INPUT_POST, $name, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY) : filter_input(INPUT_POST, $name);
            $field['default_value'] = (!isset($field['attributes']['readonly']) && !isset($field['attributes']['disabled'])) ? $sel : $field['default_value'];
          }
          else if (in_array($field['type'], array('checkboxes'))) {
            $i = 0;
            $ck = array();
            foreach ($field['options'] as $key => $val) {
              $ck[$key] = filter_input(INPUT_POST, "{$name}_{$i}");
              $i++;
            }
            $field['default_value'] = (!isset($field['attributes']['readonly']) && !isset($field['attributes']['disabled'])) ? $ck : intval($field['default_value']);
          }
          else if (in_array($field['type'], array('radio', 'checkbox'))) {
            $rc = (isset($_POST[$name])) ? 1 : 0;
            $field['default_value'] = (!isset($field['attributes']['readonly']) && !isset($field['attributes']['disabled'])) ? $rc : intval($field['default_value']);
          }
        }
      }
      $validation = $this->validate_form();
      if ($this->form['valid']) {
        // form is valid
        // Do a pre submit callback
        if (isset($this->form['presubmit_callback'])) {
          if (is_array($this->form['presubmit_callback'])) {
            foreach ($this->form['presubmit_callback'] as $callback) {
              $callback($this->form);
            }
          }
          else {
            $this->form['presubmit_callback']($this->form);
          }
        }
        if (isset($this->form['type']) && $this->form['type'] == 'admin_settings') {
          // save to options
          foreach ($this->form as $name => &$field) {
            if (!in_array($name, $this->form_fields)) {
              update_option("{$this->form['id']}_{$name}", $field['default_value']);
            }
          }
        }
        // Do a post submit callback
        if (isset($this->form['postsubmit_callback'])) {
          if (is_array($this->form['postsubmit_callback'])) {
            foreach ($this->form['postsubmit_callback'] as $callback) {
              $callback($this->form);
            }
          }
          else {
            $this->form['postsubmit_callback']($this->form);
          }
        }
      }
      else {
        // form is not valid
        $this->form['invalid_fields'] = (isset($validation['fields'])) ? $validation['fields'] : FALSE;
        $this->form['invalid_other'] = (isset($validation['other'])) ? $validation['other'] : FALSE;
      }
    }
  }

  private function language_uri($path) {
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
    if (is_plugin_active('sitepress-multilingual-cms/sitepress.php')) {
      if (strpos($path, '?') && !strpos($path, 'lang')) {
        $path = rtrim($path, ' &/#') . '&lang=' . ICL_LANGUAGE_CODE;
      }
      else if (strpos($path, '?') && strpos($path, 'lang')) {
        $path = preg_replace('/lang=\w+/', 'lang=' . ICL_LANGUAGE_CODE, $path);
      }
      else {
        $path = rtrim($path, '/') . '/?lang=' . ICL_LANGUAGE_CODE;
      }
    }

    return $path;
  }

}

class simple_sc_form extends simple_form {

  public function render_sc_form($instance) {
    $output = parent::render_form();

    return $output;
  }

}
